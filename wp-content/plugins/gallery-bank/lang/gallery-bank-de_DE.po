msgid ""
msgstr ""
"Project-Id-Version: Gallery Bank\n"
"POT-Creation-Date: 2017-02-16 13:20+0530\n"
"PO-Revision-Date: \n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.11\n"
"X-Poedit-KeywordsList: _e;__\n"
"X-Poedit-Basepath: ..\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPathExcluded-0: assets\n"

#: front_views/gallery-bank-shortcode.php:10
msgid "Gallery Type"
msgstr "Art der Galerie"

#: front_views/gallery-bank-shortcode.php:12
msgid "Albums with Images"
msgstr "Album mit Bildern"

#: front_views/gallery-bank-shortcode.php:13
msgid "Only Images"
msgstr "Nur Bilder"

#: front_views/gallery-bank-shortcode.php:17
msgid "Album Format"
msgstr "Albumformat"

#: front_views/gallery-bank-shortcode.php:25
msgid "Choose Type"
msgstr "Typ wählen"

#: front_views/gallery-bank-shortcode.php:33
msgid "Choose Albums"
msgstr "Album mit Bildern"

#: front_views/gallery-bank-shortcode.php:50
msgid "Choose Album"
msgstr "Album"

#: front_views/gallery-bank-shortcode.php:85
msgid "Gallery Format"
msgstr "Galerieformat"

#: front_views/gallery-bank-shortcode.php:95
msgid "Text Format"
msgstr "Textformat"

#: front_views/gallery-bank-shortcode.php:105 views/album-sorting.php:190
msgid "Albums in Row"
msgstr "Alben in einer Reihe"

#: front_views/gallery-bank-shortcode.php:109 views/album-preview.php:50
#: views/images-sorting.php:240
msgid "Images in Row"
msgstr "Bilder in einer Reihe"

#: front_views/gallery-bank-shortcode.php:114
msgid "Animation Effects"
msgstr "Animationseffekte"

#: front_views/gallery-bank-shortcode.php:119
msgid "Special Effects"
msgstr "Spezialeffekte"

#: front_views/gallery-bank-shortcode.php:130
msgid "Display Images"
msgstr "Bilder in einer Reihe"

#: front_views/gallery-bank-shortcode.php:136
msgid "Sort By"
msgstr "Sortieren bei"

#: front_views/gallery-bank-shortcode.php:148
msgid "No. of Images"
msgstr "Bilder in einer Reihe"

#: front_views/gallery-bank-shortcode.php:152
msgid "Show Responsive Gallery"
msgstr "Zeige reagierende Galerie"

#: front_views/gallery-bank-shortcode.php:154
msgid "Show Album Title"
msgstr "Zeige Albumname"

#: front_views/gallery-bank-shortcode.php:159
msgid "Insert Album"
msgstr "Füge Album ein"

#: front_views/gallery-bank-shortcode.php:160
msgid "Cancel"
msgstr "Abbrechen"

#: front_views/gallery-bank-shortcode.php:340
msgid "Enter number of images you want to display."
msgstr "Geben Sie die Anzahl der Bilder, die Sie anzeigen möchten."

#: gallery-bank.php:137 gallery-bank.php:213 gallery-bank.php:291
msgid "<img src=\""
msgstr "<img src=\""

#: gallery-bank.php:139 gallery-bank.php:215 gallery-bank.php:293
msgid "/wp-admin/admin.php?page=gallery_bank"
msgstr "/wp-admin/admin.php?page=gallery_bank"

#: gallery-bank.php:146 gallery-bank.php:222 gallery-bank.php:300
#: lib/gallery-bank-class.php:24 lib/gallery-bank-class.php:41
#: lib/gallery-bank-class.php:58 views/header.php:305 views/header.php:319
#: views/header.php:333
msgid "Dashboard"
msgstr "Dashboard"

#: gallery-bank.php:153 gallery-bank.php:229 gallery-bank.php:307
#: lib/gallery-bank-class.php:25 lib/gallery-bank-class.php:42
#: lib/gallery-bank-class.php:59 views/dashboard.php:339 views/header.php:306
#: views/header.php:320 views/header.php:334
msgid "Short-Codes"
msgstr "Kurz-Codes"

#: gallery-bank.php:160 gallery-bank.php:236 gallery-bank.php:314
#: lib/gallery-bank-class.php:26 lib/gallery-bank-class.php:43
#: lib/gallery-bank-class.php:60 views/header.php:307 views/header.php:321
#: views/header.php:335
msgid "Album Sorting"
msgstr "Sortierung Album"

#: gallery-bank.php:167 gallery-bank.php:243 gallery-bank.php:321
#: lib/gallery-bank-class.php:27 lib/gallery-bank-class.php:44
#: lib/gallery-bank-class.php:61 views/header.php:308 views/header.php:322
#: views/header.php:336 views/settings.php:276
msgid "Global Settings"
msgstr "allgemeine EInstellungen"

#: gallery-bank.php:174 gallery-bank.php:250 gallery-bank.php:328
#: lib/gallery-bank-class.php:28 lib/gallery-bank-class.php:45
#: lib/gallery-bank-class.php:62 views/automatic-plugin-update.php:28
#: views/automatic-plugin-update.php:32
msgid "Plugin Updates"
msgstr "Plugin Updates"

#: gallery-bank.php:180 gallery-bank.php:257 gallery-bank.php:335
#: lib/gallery-bank-class.php:29 lib/gallery-bank-class.php:46
#: lib/gallery-bank-class.php:63 views/gallery-feedback.php:34
msgid "Feature Requests"
msgstr "Funktionswünsche"

#: gallery-bank.php:186 gallery-bank.php:264 gallery-bank.php:342
#: lib/gallery-bank-class.php:30 lib/gallery-bank-class.php:47
#: lib/gallery-bank-class.php:64 views/gallery-bank-system-report.php:29
#: views/header.php:309 views/header.php:323
msgid "System Status"
msgstr "Systemstatus"

#: gallery-bank.php:193 gallery-bank.php:271 gallery-bank.php:349
#: lib/gallery-bank-class.php:31 lib/gallery-bank-class.php:48
#: lib/gallery-bank-class.php:65 views/header.php:197 views/header.php:310
#: views/header.php:324 views/header.php:337
msgid "Recommendations"
msgstr "Empfehlungen"

#: gallery-bank.php:200 gallery-bank.php:278 gallery-bank.php:356
#: lib/gallery-bank-class.php:32 lib/gallery-bank-class.php:49
#: lib/gallery-bank-class.php:66 views/header.php:311 views/header.php:325
#: views/header.php:338 views/purchase_pro_version.php:29
msgid "Premium Editions"
msgstr "Prämie Auflagen"

#: gallery-bank.php:207 gallery-bank.php:285 gallery-bank.php:363
#: lib/gallery-bank-class.php:33 lib/gallery-bank-class.php:50
#: lib/gallery-bank-class.php:67 views/header.php:202 views/header.php:312
#: views/header.php:326 views/header.php:339 views/other-services.php:29
msgid "Our Other Services"
msgstr "Unseren anderen Dienstleistungen"

#: gallery-bank.php:376
msgid "View Gallery Bank Documentation"
msgstr "anzeigen Gallery Bank Dokumentation"

#: gallery-bank.php:376
msgid "Docs"
msgstr "Doc"

#: gallery-bank.php:377
msgid "View Gallery Bank Premium Editions"
msgstr "anzeigen Gallery Bank Prämie Auflagen"

#: gallery-bank.php:377
msgid "Go for Premium!"
msgstr "Gehen Sie für Prämie!"

#: lib/add-new-album-class.php:144 views/edit-album.php:355
#: views/edit-album.php:362
msgid " Set as Album Cover"
msgstr "als Albumcover einsetzen"

#: lib/add-new-album-class.php:146 views/edit-album.php:303
#: views/edit-album.php:368
msgid "Enter your Title"
msgstr "Geben Sie den Titel ein"

#: lib/add-new-album-class.php:147 views/edit-album.php:308
#: views/edit-album.php:373
msgid "Enter your Description "
msgstr "Geben Sie die Beschreibung ein"

#: lib/add-new-album-class.php:149 views/edit-album.php:314
#: views/edit-album.php:379
msgid "Enter your Tags"
msgstr "Geben Sie Tags ein"

#: lib/add-new-album-class.php:153 views/edit-album.php:400
msgid "Delete Image"
msgstr "Bild löschen"

#: lib/gallery-bank-class.php:23 lib/gallery-bank-class.php:40
#: lib/gallery-bank-class.php:57 views/header.php:293
msgid "Gallery Bank"
msgstr "Gallery Bank"

#: lib/gallery-bank-class.php:584
msgid "Add Gallery using Gallery Bank"
msgstr "Anfügen einer Galerie mit Gallery Bank"

#: views/album-preview.php:34
msgid "Album Preview"
msgstr "Album Vorschau"

#: views/album-preview.php:37 views/album-sorting.php:158
#: views/edit-album.php:161 views/edit-album.php:424
#: views/gallery-feedback.php:37 views/images-sorting.php:202
#: views/other-services.php:33 views/purchase_pro_version.php:33
#: views/settings.php:279 views/shortcode.php:33
msgid "Back to Albums"
msgstr "zurück zu den Alben"

#: views/album-preview.php:55 views/album-sorting.php:195
#: views/images-sorting.php:244
msgid "Please Choose"
msgstr "Bitte wählen"

#: views/album-sorting.php:154
msgid "Re-Order Albums"
msgstr "Alben re-organisieren"

#: views/album-sorting.php:160 views/images-sorting.php:204
msgid "Update Order"
msgstr "Reihenfolge aktualisieren"

#: views/album-sorting.php:163 views/images-sorting.php:207
msgid "Sorting Order has been updated."
msgstr "Reihenfolge der Sortierung wurde aktualisiert."

#: views/album-sorting.php:275 views/dashboard.php:487 views/dashboard.php:491
#: views/dashboard.php:495 views/edit-album.php:617 views/edit-album.php:637
#: views/images-sorting.php:308
msgid "This feature is only available in Premium Editions!"
msgstr "Diese Funktion ist nur in Premium Edition!"

#: views/automatic-plugin-update.php:35 views/settings.php:752
#: views/settings.php:760 views/settings.php:1170 views/settings.php:1178
#: views/settings.php:1196 views/settings.php:1203 views/settings.php:1221
#: views/settings.php:1228 views/settings.php:1245 views/settings.php:1252
#: views/settings.php:1360 views/settings.php:1367 views/settings.php:1400
#: views/settings.php:1408 views/settings.php:1451 views/settings.php:1459
msgid "Enable"
msgstr "Aktivieren"

#: views/automatic-plugin-update.php:36 views/settings.php:755
#: views/settings.php:763 views/settings.php:1173 views/settings.php:1181
#: views/settings.php:1198 views/settings.php:1206 views/settings.php:1223
#: views/settings.php:1230 views/settings.php:1247 views/settings.php:1254
#: views/settings.php:1362 views/settings.php:1370 views/settings.php:1403
#: views/settings.php:1411 views/settings.php:1454 views/settings.php:1462
msgid "Disable"
msgstr "Deaktivieren"

#: views/dashboard.php:290 views/edit-album.php:203
#, php-format
msgid ""
"<b>Notice:</b> Your server allows you to upload <b>%s</b> files of maximum "
"total <b>%s</b> bytes and allows <b>%s</b> seconds to complete."
msgstr ""

#: views/dashboard.php:291 views/edit-album.php:204
msgid ""
"<br />If your request exceeds these limitations, it will fail, probably "
"without an errormessage."
msgstr ""

#: views/dashboard.php:292 views/edit-album.php:205
msgid ""
"<br />Additionally your hosting provider may have set other limitations on "
"uploading files."
msgstr ""

#: views/dashboard.php:303
msgid "Gallery Bank Dashboard"
msgstr "Gallery Bank Instrumententafel"

#: views/dashboard.php:314
msgid "Add New Album"
msgstr "neues Album hinzufügen"

#: views/dashboard.php:320
msgid "Delete All Albums"
msgstr "Alle Alben löschen"

#: views/dashboard.php:321
msgid "Purge Images & Albums"
msgstr "Bereinigen der Bilder und Alben"

#: views/dashboard.php:322
msgid "Restore Factory Settings"
msgstr "Wiederherstellen der Werkseinstellungen"

#: views/dashboard.php:329
msgid "Existing Albums Overview"
msgstr "Überblick der bestehenden Alben"

#: views/dashboard.php:335 views/edit-album.php:262
msgid "Thumbnail"
msgstr "Vorschaubilder"

#: views/dashboard.php:336
msgid "Title"
msgstr "Titel"

#: views/dashboard.php:337
msgid "Total Images"
msgstr "Anzahl Bilder"

#: views/dashboard.php:338
msgid "Date"
msgstr "Datum"

#: views/dashboard.php:403
msgid "Edit Album"
msgstr "Album bearbeiten"

#: views/dashboard.php:408 views/images-sorting.php:198
msgid "Re-Order Images"
msgstr "Bilder re-organisieren"

#: views/dashboard.php:413
msgid "Preview Album"
msgstr "Albumvorschau"

#: views/dashboard.php:418
msgid "Delete Album"
msgstr "Album löschen"

#: views/dashboard.php:474
msgid "Are you sure you want to delete this Album?"
msgstr "Sind Sie sicher, dass Sie dieses Album löschen wollen?"

#: views/edit-album.php:158
msgid "Album"
msgstr "Album"

#: views/edit-album.php:162 views/edit-album.php:423
msgid "Save Album"
msgstr "Speichern Album"

#: views/edit-album.php:166
msgid "Album Saved. Kindly wait for the redirect to happen."
msgstr "Album gespeichert. Bitte warten Sie, bis die Umleitung zu geschehen."

#: views/edit-album.php:173
msgid "Album Details"
msgstr "Album Details"

#: views/edit-album.php:177
msgid "Album Title"
msgstr "Album Titel"

#: views/edit-album.php:182
msgid "Enter your Album Title"
msgstr "Geben Sie den Titel des Albums"

#: views/edit-album.php:189
msgid "Description"
msgstr "Beschreibung"

#: views/edit-album.php:213
msgid "Upload Images"
msgstr "Bilder hochladen"

#: views/edit-album.php:216
msgid "Your browser doesn\"t have Flash, Silverlight or HTML5 support."
msgstr "Ihr Browser unterstützt nicht Flash, Silverlight oder HTML5."

#: views/edit-album.php:223
msgid "Upload Videos"
msgstr "Videos hochladen"

#: views/edit-album.php:224 views/edit-album.php:269 views/settings.php:288
#: views/settings.php:504 views/settings.php:734 views/settings.php:845
#: views/settings.php:944 views/settings.php:1266 views/settings.php:1382
#: views/settings.php:1433 views/settings.php:1485
msgid " (Available in Premium Editions)"
msgstr "(Erhältlich in Premium Edition)"

#: views/edit-album.php:229
msgid "Video Url"
msgstr "URL der Video"

#: views/edit-album.php:233
msgid "Enter your Video Url"
msgstr "Geben Sie die URL der Video"

#: views/edit-album.php:239
msgid "Upload Video"
msgstr "Video hochladen"

#: views/edit-album.php:250
msgid "Your Gallery Bank Album"
msgstr "Ihr Gallery Bank Album"

#: views/edit-album.php:259
msgid "Delete"
msgstr "Löschen"

#: views/edit-album.php:265
msgid "Title & Description"
msgstr "Titel & Beschreibung"

#: views/edit-album.php:268
msgid "Tags (comma separated list)"
msgstr "Tags (komma getrennte Liste)"

#: views/edit-album.php:272
msgid "Url to Redirect on click of an Image"
msgstr "URL für die Umleitung beim Click auf ein Bild"

#: views/edit-album.php:324
msgid "Delete Video"
msgstr "Video löschen"

#: views/edit-album.php:604
msgid "Are you sure you want to delete this Image?"
msgstr "Sind Sie sicher, dass Sie dieses Bild löschen wollen?"

#: views/gallery-bank-system-report.php:32
msgid "Get System Report"
msgstr "Systembericht erhalten"

#: views/gallery-bank-system-report.php:36
msgid "Close System Report"
msgstr "Systembericht schließen"

#: views/gallery-feedback.php:41
msgid "Email has been sent successfully."
msgstr "E-Mail  erfolgreich versandt."

#: views/gallery-feedback.php:48 views/gallery-feedback.php:64
msgid "Feature Requests / Suggestions"
msgstr "Funktionswünsche / Vorschläge"

#: views/gallery-feedback.php:52
msgid "Name"
msgstr "Name"

#: views/gallery-feedback.php:54
msgid "Enter your Name"
msgstr "Geben Sie Ihren Namen ein"

#: views/gallery-feedback.php:58
msgid "Email Id"
msgstr "E-Mail ID"

#: views/gallery-feedback.php:60
msgid "Enter your Email Address"
msgstr "Geben Sie Ihre E-Mail Adresse ein"

#: views/gallery-feedback.php:66
msgid "Enter your Feature Requests / Suggestions"
msgstr "Geben Sie Ihre Funktionswünsche / Vorschläge ein"

#: views/gallery-feedback.php:71
msgid "Send"
msgstr "Senden"

#: views/header.php:59
#, php-format
msgid ""
"Based on your server memory limit you should not upload images larger then "
"<strong>%d x %d (%2.1f MP)</strong>"
msgstr ""

#: views/header.php:134
msgid "Get Started"
msgstr "Fangen Sie an"

#: views/header.php:137
msgid "Watch Gallery Video!"
msgstr "beobachten Gallery Video!"

#: views/header.php:141
msgid "read documentation here"
msgstr "lesen Dokumentation Sie hier"

#: views/header.php:146
msgid "Go Premium"
msgstr "Gehen Prämie"

#: views/header.php:150
msgid "Features"
msgstr "Eigenschaften"

#: views/header.php:155
msgid "Online Demos"
msgstr "Live-Beispiele"

#: views/header.php:160
msgid "Pricing Plans"
msgstr "Contact Bank Preise"

#: views/header.php:167
msgid "Knowledge Base"
msgstr "Wissensdatenbank"

#: views/header.php:172
msgid "Support Forum"
msgstr "Technischer Hilfsdienst"

#: views/header.php:177
msgid "FAQ's"
msgstr "Häufig gestellte Fragen"

#: views/header.php:182
msgid "Renew Premium Support"
msgstr "Erneuern Premium Support"

#: views/header.php:188
msgid "More Actions"
msgstr "Weitere Aktionen"

#: views/header.php:192
msgid "Plugin Customization"
msgstr "Plugin Anpassungs"

#: views/header.php:226 views/header.php:257
msgid ""
"There is a serious misconfiguration in your servers PHP config. Function "
"imagecreatefromjpeg() does not exist. You will encounter problems when "
"uploading photos and not be able to generate thumbnail images. Ask your "
"hosting provider to add GD support with a minimal version 1.8."
msgstr ""

#: views/header.php:232 views/header.php:237 views/header.php:263
#: views/header.php:268
msgid "unknown"
msgstr ""

#: views/purchase_pro_version.php:38
msgid "WP Gallery Bank is an one time Investment. Its Worth it!"
msgstr "WP Gallery Bank ist eine einmalige Investition. Es lohnt sich!"

#: views/recommended-plugins.php:67
#, php-format
msgid "%s rating"
msgstr ""

#: views/recommended-plugins.php:135
msgid "More Details"
msgstr ""

#: views/recommended-plugins.php:146
msgid "Install Now"
msgstr ""

#: views/recommended-plugins.php:153
msgid "Update Now"
msgstr ""

#: views/recommended-plugins.php:281
msgid "<strong>Untested</strong> with your version of WordPress"
msgstr ""

#: views/recommended-plugins.php:285
msgid "Incompatible with your version of WordPress"
msgstr ""

#: views/recommended-plugins.php:289
msgid "Compatible with your version of WordPress"
msgstr ""

#: views/settings.php:280
msgid "Update Settings"
msgstr "Einstellungen aktualisieren"

#: views/settings.php:287
msgid "Thumbnail Settings"
msgstr "Einstellungen Vorschaubilder"

#: views/settings.php:299
msgid "Thumbnail Size"
msgstr "Größe Vorschaubilder"

#: views/settings.php:306 views/settings.php:314 views/settings.php:522
#: views/settings.php:530
msgid "Original"
msgstr "Original"

#: views/settings.php:309 views/settings.php:317 views/settings.php:525
#: views/settings.php:533
msgid "Custom"
msgstr "Spezifisch"

#: views/settings.php:326 views/settings.php:542
msgid "Width"
msgstr "Breite"

#: views/settings.php:336 views/settings.php:552
msgid "Height"
msgstr "Höhe"

#: views/settings.php:348 views/settings.php:563 views/settings.php:985
msgid "Opacity"
msgstr "Deckkraft"

#: views/settings.php:360 views/settings.php:575 views/settings.php:996
msgid "Border Size"
msgstr "Rahmengröße"

#: views/settings.php:371 views/settings.php:587 views/settings.php:1007
msgid "Border Radius"
msgstr "Rahmenradius"

#: views/settings.php:382 views/settings.php:599 views/settings.php:1032
msgid "Border Color"
msgstr "Rahmenfarbe"

#: views/settings.php:396
msgid "Margin Between Images"
msgstr "Platz zwischen den Bildern"

#: views/settings.php:406 views/settings.php:633 views/settings.php:1018
msgid "Text Color"
msgstr "Textfarbe"

#: views/settings.php:420 views/settings.php:650 views/settings.php:1092
msgid "Text-Align"
msgstr "Textausrichtung"

#: views/settings.php:434 views/settings.php:664 views/settings.php:801
#: views/settings.php:1106 views/settings.php:1314
msgid "Font-Family"
msgstr "Schriftartfamilie"

#: views/settings.php:457 views/settings.php:687 views/settings.php:1129
msgid "Heading Font-Size"
msgstr "Überschrift Schriftartgröße"

#: views/settings.php:474 views/settings.php:704 views/settings.php:1146
msgid "Text Font-Size"
msgstr "Text Schriftartgröße"

#: views/settings.php:491 views/settings.php:721
msgid "Description Length"
msgstr "Länge Beschreibung"

#: views/settings.php:503
msgid "Album Cover Settings"
msgstr "Einstellungen Titelbild Album"

#: views/settings.php:515
msgid "Cover Size"
msgstr "Titelbildgröße"

#: views/settings.php:613
msgid "Margin Between Albums"
msgstr "Platz zwischen den Alben"

#: views/settings.php:623
msgid "Text for Album Click"
msgstr "Text für Klick auf dem Album"

#: views/settings.php:733
msgid "Filter Settings"
msgstr "Filtereinstellungen"

#: views/settings.php:745
msgid "Filters"
msgstr "Filters"

#: views/settings.php:773
msgid "Filter Color"
msgstr "Filterfarbe"

#: views/settings.php:787
msgid "Filter Text Color"
msgstr "Filter Textfarbe"

#: views/settings.php:824 views/settings.php:1337
msgid "Font-Size"
msgstr "Schriftartgröße"

#: views/settings.php:844
msgid "Roles & Capabilities"
msgstr "Rolen und Möglichkeiten"

#: views/settings.php:856
msgid "Privileges for Admin"
msgstr "Rechte für Administratoren"

#: views/settings.php:860 views/settings.php:876 views/settings.php:892
#: views/settings.php:909 views/settings.php:927
msgid "Full Control"
msgstr "Full Control"

#: views/settings.php:863 views/settings.php:879 views/settings.php:895
#: views/settings.php:912 views/settings.php:930
msgid "Read"
msgstr "Lesen"

#: views/settings.php:866 views/settings.php:882 views/settings.php:898
#: views/settings.php:915 views/settings.php:933
msgid "Write"
msgstr "Schreiben"

#: views/settings.php:872
msgid "Privileges for Editor"
msgstr "Rechte für Bearbeiter"

#: views/settings.php:888
msgid "Privileges for Author"
msgstr "Rechte für Autoren"

#: views/settings.php:904
msgid "Privileges for Contributor"
msgstr "Rechte für Redakteur"

#: views/settings.php:921
msgid "Privileges for Subscriber"
msgstr "Rechte für Abonnent"

#: views/settings.php:943
msgid "Lightbox Settings"
msgstr "Einstellungen Lightbox"

#: views/settings.php:955
msgid "Lightbox Type"
msgstr "Typ Lightbox"

#: views/settings.php:1046
msgid "Inline Background"
msgstr "Inline Hintergrund"

#: views/settings.php:1060
msgid "Overlay Background"
msgstr "Overlay Hintergrund"

#: views/settings.php:1074
msgid "Fade In Time"
msgstr "Einblendungszeit"

#: views/settings.php:1083
msgid "Fade Out Time"
msgstr "Ausblendungszeit"

#: views/settings.php:1163
msgid "Facebook Comments"
msgstr "Facebook Kommentare"

#: views/settings.php:1190
msgid "Social Sharing"
msgstr "Teilen in SocialMedia"

#: views/settings.php:1215
msgid "Image Title"
msgstr "Bildtitel"

#: views/settings.php:1239
msgid "Image Description"
msgstr "Bild Beschreibung"

#: views/settings.php:1265
msgid "Front - End Layout Settings"
msgstr "Einstellungen Front-end Layout"

#: views/settings.php:1277
msgid "Text for Back Button"
msgstr "Text für Zurück-Taste"

#: views/settings.php:1287
msgid "Button Color"
msgstr "Farbe Taste"

#: views/settings.php:1300
msgid "Button Text Color"
msgstr "Taste Textfarbe "

#: views/settings.php:1354
msgid "Album Seperator"
msgstr "Album Trenner"

#: views/settings.php:1381
msgid "Pagination Settings for Images"
msgstr "Parameter der Pagination der Bilder"

#: views/settings.php:1393
msgid "Paging"
msgstr "Seitennummerierung"

#: views/settings.php:1420
msgid "No. of Images Per Page "
msgstr "Anzahl der Bilder pro Seite"

#: views/settings.php:1432
msgid "Slide Show Settings"
msgstr "Diashow Einstellungen"

#: views/settings.php:1444
msgid "Auto Play"
msgstr "Autoplay"

#: views/settings.php:1471
msgid "Interval"
msgstr "Intervall"

#: views/settings.php:1484
msgid "Language Direction Settings"
msgstr "Einstellungen xxx"

#: views/settings.php:1496
msgid "Language Direction"
msgstr "Sprachrichtung"

#: views/settings.php:1653
msgid "These features are only available in Premium Editions!"
msgstr "(Premiumeigenschaften nur in der kostenpflichtigen Version)"

#: views/shortcode.php:29
msgid "Gallery Bank Short-Codes"
msgstr "Gallery Bank Kurz-Codes"

#~ msgid " (Available only in Premium Editions)"
#~ msgstr "(Premiumeigenschaften nur in der kostenpflichtigen Version)"

#~ msgid "Insert Gallery Bank Shortcode"
#~ msgstr "Füge \"Gallery Bank Shortcode\" ein"

#~ msgid "Select an album below to add it to your post or page."
#~ msgstr ""
#~ "Wähle ein Album aus, um es einem Beitrag oder einer Seite hinzuzufügen"

#~ msgid "Select Album Format"
#~ msgstr "Wähle ein Albumformat"

#~ msgid "Select Album"
#~ msgstr "Wähle Album aus"

#~ msgid "Select an Album"
#~ msgstr "Wähle ein Album aus"

#~ msgid "Select Gallery Format "
#~ msgstr "Wähle ein Galerieformat aus"

#~ msgid "Image Width"
#~ msgstr "Bildbreite"

#~ msgid "Select Format "
#~ msgstr "Wähle ein Format aus"

#~ msgid "Please select an Album"
#~ msgstr "Bitte wähle ein Album aus"

#~ msgid "Please select an Album Format"
#~ msgstr "Bitte wähle ein Albumformat"

#~ msgid "Please select a Gallery Images Format"
#~ msgstr "Bitte wähle ein Galere-Bilder-Format"

#~ msgid "Please select a Text Format for the Gallery"
#~ msgstr "Bitte wähle eine Textformatierung für die Galerie"

#~ msgid "This feature is only available in Paid Premium Version!"
#~ msgstr "Diese Merkmal ist nur in Paid verfügbar Prämie Fassung!"

#~ msgid " (Available in Premium Versions)"
#~ msgstr "(Verfügbar in Premium-Versionen)"

#~ msgid "Default"
#~ msgstr "Standardwert"

#~ msgid "Premium Pricing Plans"
#~ msgstr "PrämiePreispläne"

#~ msgid "Detailed Features"
#~ msgstr "ausführliche Eigenschaften"

#~ msgid "This Feature is only available in Paid Premium Version!"
#~ msgstr "Diese Merkmal ist nur in Paid verfügbar Prämie Fassung!"

#~ msgid "Purchase Pro Version"
#~ msgstr "Kaufen Sie Pro Fassung"

#~ msgid "Save and Submit Changes"
#~ msgstr "Speichern und Änderungen übermitteln"

#~ msgid "Album Published. Kindly wait for the redirect to happen."
#~ msgstr "Album veröffentlicht. Bitte warten für die Umleitung."

#~ msgid ""
#~ "You will be only allowed to add 2 galleries. Kindly purchase Premium "
#~ "Version for full access!"
#~ msgstr ""
#~ "Sie werden nur erlaubt sein, zwei Galerien hinzufügen. Bitte kaufen "
#~ "Prämie Fassung  für vollen Zugriff!"

#~ msgid "VIEW ALL"
#~ msgstr "Alle ansehen"

#~ msgid "Licensing"
#~ msgstr "Lizensierung"

#~ msgid " Rotate Image"
#~ msgstr "Bild drehen"

#~ msgid "Select Gallery"
#~ msgstr "Galerie auswählen"

#~ msgid "Select Gallery Format"
#~ msgstr "Format der Galerie auswählen"

#~ msgid "Images In Row"
#~ msgstr "Bilder in Reihe"

#~ msgid "Select Text Format"
#~ msgstr "Text Format auswählen"

#~ msgid "Special Effect"
#~ msgstr "besonderen Eindruck "

#~ msgid "Select Special Effect"
#~ msgstr "besonderen Eindruck auswählen"

#~ msgid "Animation Effect"
#~ msgstr "Eindruck der Animation"

#~ msgid "Responsive Gallery"
#~ msgstr "responsive Galerie"

#~ msgid "Upload Images "
#~ msgstr "Bilder hochladen"

#~ msgid "Your Wordpress Version doesn\"t support Wp-media."
#~ msgstr "Ihre Wordpress-Version nicht unterstützt Wp-Media."

#~ msgid "Are you sure you want to delete these Images?"
#~ msgstr "Sind Sie sicher, dass Sie diese Bilder löschen wollen?"

#~ msgid "Attention! A new vesion of Gallery Bank is available for download."
#~ msgstr ""
#~ "Vorsicht! Eine neue Version von Gallery Bank ist zum Download verfügbar."

#~ msgid "Click"
#~ msgstr "Klicken Sie"

#~ msgid "here"
#~ msgstr "hier"

#~ msgid "to upgrade your version of Gallery Bank."
#~ msgstr "um ihre Gallery Bank version zu aktualisieren."

#~ msgid "Dashboard - Gallery Bank"
#~ msgstr "Dashboard - Gallery Bank"

#~ msgid "Date of Creation"
#~ msgstr "Herstellungsdatum"

#~ msgid "Are you sure you want to delete all Albums?"
#~ msgstr "Sind Sie sicher, dass Sie diese Alben löschen wollen?"

#~ msgid "Are you sure you want to Restore Factory Settings?"
#~ msgstr ""
#~ "Sind Sie sicher, dass Sie die Werkseinstellungen wiederherstellen wollen?"

#~ msgid "Are you sure you want to Purge all Images & Albums?"
#~ msgstr "Sind Sie sicher, dass Sie alle Bilder und Alben bereinigen wollen?"

#~ msgid "Upload Thumbnails "
#~ msgstr "hochladen Miniaturansichten"

#~ msgid "Gallery Bank Licensing Module"
#~ msgstr "Lizenzmodul Gallery Bank"

#~ msgid "Update"
#~ msgstr "Aktualisieren"

#~ msgid "Success! Settings have been updated."
#~ msgstr "Erfolgreich! Einstellungen wurden aktualisiert."

#~ msgid "Restore Settings"
#~ msgstr "Einstellungen wiederherstellen"

#~ msgid "Settings has been updated."
#~ msgstr "Einstellungen wurden aktualisiert"

#~ msgid "Success! Settings have been restored."
#~ msgstr "Erfolgreich! Einstellungen wurden wiederhergestellt."

#~ msgid "Show Albums"
#~ msgstr "Alben anzeigen"

#~ msgid "Responsive"
#~ msgstr "ansprechbar"

#~ msgid "With Albums in Row"
#~ msgstr "Mit Alben in Row"

#~ msgid "Image Uploader Settings"
#~ msgstr "Bild Uploader Einstellungen"

#~ msgid "Uploader for Images"
#~ msgstr "Uploader für Bilder"

#~ msgid "WP Media"
#~ msgstr "WP Media"

#~ msgid "Change Uploading Image Name"
#~ msgstr "ändern hochladen Bildname"

#~ msgid "Image url location"
#~ msgstr "Bild url Lage"

#~ msgid "Open in New Window"
#~ msgstr "In neuem Fenster öffnen"

#~ msgid "Same Window"
#~ msgstr "Gleichen -Fenster"

#~ msgid "Are you sure you want to restore settings?"
#~ msgstr "Sind Sie sicher, dass Sie die Einstellungen zurücksetzen wollen?"

#~ msgid "Self"
#~ msgstr "selbe Seite"

#~ msgid "Video Thumbnails"
#~ msgstr "Video Vorschaubilder"

#~ msgid "Video Thumbnail Path"
#~ msgstr "Pfad Video Vorschaubilder"

#~ msgid "Enter your Video Thumbnail Url"
#~ msgstr "Geben SIe die URL des Video Vorschaubildes"
